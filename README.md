# Docker RosarioSIS SPX: installation and exploitation guide

# Installation
## Pre-requisite
- a linux server
- a user with sudo access
- git, docker, docker compose (https://docs.docker.com/compose/install/linux/)
- the rosario service URL e.g. rosario.mydomain.fr with a DNS entry (ping rosario.mydomain.fr must respond to configure traefik)

Check that git is installed:

    git --version

Chek that docker and docker compose are installed:

    docker --version

	docker compose version

N.B. docker compose version must be greater than 2

Ping your rosario service url:

    ping rosario.mydomain.fr

THhe name must be resolved (i.e. an IP address must be given).

## Create a rosariospx user with docker group
    sudo useradd --create-home --groups docker --shell /bin/bash rosariospx

## Log on to rosariospx from the user with su rights
	sudo su - rosariospx

## Clone docker-rosariospx project

    cd /home/rosariospx
    git clone https://gitlab.com/AbEB/docker-rosariospx.git

## Rename .env.exemple in .env
.env contains variables used to configure rosario and the network domain

    cd docker-rosariospx
    cp ~/docker-rosariospx/.env.exemple ~/docker-rosariospx/.env

Edit .env file and update the environment variables with the correct version and the domain (e.g. rosario.mydomain.fr)

To use paying modules, put them in docker-rosariospx/modules.

## Start traefik to securize the service (https)
If traefik is already running, that section can be skipped. Just check that the traefik service name is t2_proxy. Update it in compose.traefik.yml otherwise.

Go to the traefik directory under docker-rosariospx and edit the docker-compose.yml to replace rosariospx@proton.me by your rosario admin email. Then launch traefik with:

    cd ~/docker-rosariospx/traefik
    docker compose up -d


## Start the rosariospx service

    cd ~/docker-rosariospx
    ./startRosario.sh

N.B. The first time the command will be launched, it will take around 10 minutes to build the image. Next launch will be faster.

N.B. The database will be saved in a persistent volume /apps/rosariospx/db

## Initialize the database
With a browser go to the following URL: https://rosario.mydomain.fr/InstallDatabase.php and select French as language

## Test default login
Go to https://rosario.mydomain.fr

user: admin
password: admin

Don't forget the new password. Eventually use a password manager such as keepassxc

## Cron the backup script
N.B. The backups are saved in /tmp/

Give backup.sh the execution right:

    chmod 755 /home/rosariospx/docker-rosariospx/exploitation/backup.sh

Edit the cron table to schedule the backup task:

    crontab -e

Add the following line:

    0 0 * * * /home/rosariospx/docker-rosariospx/exploitation/backup.sh

The backup script will run every day at 0H00

# Exploitation
## Relaunching rosario after a server reboot

Start traefik if it's managed by this project:

    cd ~/docker-rosariospx/traefik
    docker compose up -d

Start rosario:

    cd ~/docker-rosariospx
    ./startRosario.sh

## Update Rosario with a new version
Backup the database:

    cd ~/docker-rosariospx/exploitation/
    ./backup.sh

Edit the file ~/docker-rosariospx/.env and put the new version:

ROSARIOSIS_VERSION=new version

    ./startRosario.sh

## Update modules and plugins
Edit the file ~/docker-rosariospx/.env and increments the version:

SPX_VERSION=old_version+1

    ./startRosario.sh

## Database and asset directories restore
To restore a backup (DB and assets directories), the user with the sudo right must be used and needs to run:

    cd /home/rosariospx/docker-rosariospx/exploitation
    ./restore.sh

## Importation of a database
Be careful that the user, the dbname, the login and the default year are good.

cat /home/rosariospx/rosario.dump | docker exec -i $(docker ps -q --filter name=rosariospx-db) pg_restore --username=rosario --dbname=rosariosis --clean --verbose

## Copy of the folder /apps
Be careful that the user, the dbname, the login and the default year are good.

Install docker-rosariospx as usual, paying attention to the user, the dbname, the login and the default year are good.

Installs the database as usual, with InstallDatabase.php.

Finaly, copy the folder /apps with cp -rp /my/old/apps/* /apps/ 

