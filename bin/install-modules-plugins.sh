#!/bin/bash

IFS=';'

# Retrieving the modules
cd /usr/src/rosariosis/modules/

for module in $MODULES
do
  git clone https://gitlab.com/$module
done

# Retrieving the plugins
cd /usr/src/rosariosis/plugins/

for plugin in $PLUGINS
do
  git clone https://gitlab.com/$plugin
done

# Retrieving the themes
cd /usr/src/rosariosis/assets/themes/

for theme in $THEMES
do
  git clone https://gitlab.com/$theme
done

# Retrieving the locale
if [ $LOCALE != "" ] 
then
  rm -r /usr/src/rosariosis/locale/$ROSARIOSIS_LANG.utf8
  cd /usr/src/rosariosis/locale/

  git clone https://gitlab.com/$LOCALE $ROSARIOSIS_LANG.utf8
fi
