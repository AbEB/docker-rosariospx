#!/bin/bash

# If no argument is passed, configure traefik
SPECIFIC_CONFIG=compose.traefik.yml

if [ "$1" == "dev" ]; then
    SPECIFIC_CONFIG=compose.dev.yml
fi

docker compose \
    --env-file .env \
    -f compose.yml \
    -f ${SPECIFIC_CONFIG} \
    up --build --detach
