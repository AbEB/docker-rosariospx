# Dockerfile for RosarioSIS
# https://www.rosariosis.org/
# Best Dockerfile practices: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

# https://hub.docker.com/_/php?tab=tags&page=1&name=apache
# TODO When moving to PHP8.0, remove xmlrpc extension & libxml2-dev!
FROM php:7.4-apache-bullseye

LABEL maintainer="Houat"

ENV DBTYPE=postgresql \
    PGPORT=5432

# Install postgresql-client, sendmail, nano editor, locales
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        postgresql-client sendmail nano locales git vim wkhtmltopdf;

# Download and install wkhtmltopdf (avoid direct installation via apt, saves 115M :)
#RUN curl -L https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-2/wkhtmltox_0.12.6.1-2.bullseye_amd64.deb \
#        --output wkhtmltox_0.12.6.1-2.bullseye_amd64.deb; \
#    apt-get install -y --no-install-recommends ./wkhtmltox_0.12.6.1-2.bullseye_amd64.deb; \
#    rm wkhtmltox_0.12.6.1-2.bullseye_amd64.deb;

# Install PHP extensions build dependencies
# Note: $savedAptMark var must be assigned & used in the same RUN command.
RUN savedAptMark="$(apt-mark showmanual)"; \
    apt-get install -y --no-install-recommends \
        libicu-dev libpq-dev libjpeg-dev libpng-dev libldap2-dev libxml2-dev libzip-dev libonig-dev; \
    \
    # Install PHP extensions (curl, mbstring & xml are already included).
    docker-php-ext-configure gd --with-jpeg; \
    debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)"; \
    docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch"; \
    docker-php-ext-install -j$(nproc) gd pgsql gettext intl xmlrpc zip ldap; \
    \
    # Reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    extDir="$(php -r 'echo ini_get("extension_dir");')"; \
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    # Remove compilers & dev libraries
    apt-mark auto autoconf dpkg-dev g++ gcc libc6-dev make; \
    ldd "$extDir"/*.so \
        | awk '/=>/ { print $3 }' \
        | sort -u \
        | xargs -r dpkg-query -S \
        | cut -d: -f1 \
        | sort -u \
        | xargs -rt apt-mark manual; \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /var/lib/apt/lists/*;

# Set recommended PHP.ini settings
RUN { \
    echo 'max_execution_time = 240'; \
    echo 'max_input_vars = 4000'; \
    echo 'memory_limit = 512M'; \
    echo 'session.gc_maxlifetime = 3600'; \
    echo 'upload_max_filesize = 50M'; \
    echo 'post_max_size = 51M'; \
} > /usr/local/etc/php/conf.d/rosariosis-recommended.ini
# Set recommended PHP error logging
RUN { \
# https://www.php.net/manual/en/errorfunc.constants.php
    echo 'error_reporting = E_ERROR | E_WARNING | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING | E_RECOVERABLE_ERROR'; \
    echo 'display_errors = Off'; \
    echo 'display_startup_errors = Off'; \
    echo 'log_errors = On'; \
    echo 'error_log = /dev/stderr'; \
    echo 'log_errors_max_len = 1024'; \
    echo 'ignore_repeated_errors = On'; \
    echo 'ignore_repeated_source = Off'; \
    echo 'html_errors = Off'; \
} > /usr/local/etc/php/conf.d/error-logging.ini

#RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

# Download and extract rosariosis
# Change le comportement par défaut de clone SSH en clone https, puis clone le dépôt récursivement
RUN mkdir /usr/src/rosariosis  && \
    git config --global url."https://gitlab.com/".insteadOf git@gitlab.com: && \
    git clone https://gitlab.com/francoisjacquet/rosariosis.git /usr/src/rosariosis


ARG MODULES
ARG PLUGINS
ARG THEMES
ARG LOCALE

# Checkout specific tag if different from latest
ARG ROSARIOSIS_VERSION
ARG ROSARIOSIS_YEAR 
ARG ROSARIOSIS_LANG
ARG SPX_VERSION

RUN cd /usr/src/rosariosis && git pull && git checkout -f $ROSARIOSIS_VERSION

# Copy locales modules from permanent folder to image
# RUN cp /apps/rosariospx/modules /usr/src/rosariosis/modules
ADD modules /usr/src/rosariosis/modules
RUN rm /usr/src/rosariosis/favicon.ico
ADD bin/favicon.ico /usr/src/rosariosis/


COPY bin/install-modules-plugins.sh /
RUN chmod 755 /install-modules-plugins.sh && /install-modules-plugins.sh

# Copy our configuration files.
COPY conf/config.inc.php /usr/src/rosariosis/config.inc.php
COPY bin/init /init

EXPOSE 80

ENTRYPOINT ["/init"]
CMD ["apache2-foreground"]
